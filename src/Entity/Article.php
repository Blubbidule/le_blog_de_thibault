<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    const SECTIONS = ['Critiques','Découvertes','Bizarreries'];
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre_article;

    /**
     * @ORM\Column(type="text")
     */
    private $texte_article;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image_article;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Choice(choices=Article::SECTIONS, message="Le nom de section choisi est invalide")
     */
    private $section;

    /**
     * @ORM\OneToMany(targetEntity=Commentaires::class, mappedBy="id_article")
     */
    private $commentaires;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $textePresentation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sousTitreArticle;

    /**
     * @ORM\OneToOne(targetEntity=Roman::class, mappedBy="idArticle", cascade={"persist", "remove"})
     */
    private $romanId;

    public function __construct()
    {
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreArticle(): ?string
    {
        return $this->titre_article;
    }

    public function setTitreArticle(string $titre_article): self
    {
        $this->titre_article = $titre_article;

        return $this;
    }

    public function getTexteArticle(): ?string
    {
        return $this->texte_article;
    }

    public function setTexteArticle(string $texte_article): self
    {
        $this->texte_article = $texte_article;

        return $this;
    }

    public function getImageArticle(): ?string
    {
        return $this->image_article;
    }

    public function setImageArticle(string $image_article): self
    {
        $this->image_article = $image_article;

        return $this;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(string $section): self
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return Collection|Commentaires[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaires $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setIdArticle($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaires $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getIdArticle() === $this) {
                $commentaire->setIdArticle(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTextePresentation(): ?string
    {
        return $this->textePresentation;
    }

    public function setTextePresentation(string $textePresentation): self
    {
        $this->textePresentation = $textePresentation;

        return $this;
    }

    public function getSousTitreArticle(): ?string
    {
        return $this->sousTitreArticle;
    }

    public function setSousTitreArticle(string $sousTitreArticle): self
    {
        $this->sousTitreArticle = $sousTitreArticle;

        return $this;
    }

    public function getRomanId(): ?Roman
    {
        return $this->romanId;
    }

    public function setRomanId(Roman $romanId): self
    {
        $this->romanId = $romanId;

        // set the owning side of the relation if necessary
        if ($romanId->getIdArticle() !== $this) {
            $romanId->setIdArticle($this);
        }

        return $this;
    }
}

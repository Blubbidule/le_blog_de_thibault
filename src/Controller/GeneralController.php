<?php

namespace App\Controller;

use App\Entity\ContactMessage;
use App\Form\ArticleFormType;
use App\Form\CommentsType;
use App\Form\ContactType;
use App\Form\SearchDataForm;
use App\Form\SearchFormType;
use App\Form\SearchType;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Repository\CommentairesRepository;
use App\Repository\RomanRepository;
use App\Search\SearchData;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Commentaires;

class GeneralController extends AbstractController
{
    /**
     * page d'accueil
     * @Route("/", name="homePage")
     */
    public function home(Request $request,PaginatorInterface $paginator): Response
    {
        $artRepo = $this->getDoctrine()->getRepository(Article::class);
        $articles = $artRepo->findAllByRecent();

        $articles = $paginator->paginate(
            $articles,
            $request->query->getInt('page', 1),
            3
        );

        return $this->render('pages/index.html.twig', [
            'pageTitle' => 'Accueil',
            'articlesPage' => $articles,
        ]);
    }

    /**
     * page de contact
     * @Route("/contact", name="contactPage")
     */
    public function contact(Request $request): Response
    {
        $contactMessage = new ContactMessage();
        $contactForm = $this->createForm(ContactType::class, $contactMessage);

        $contactForm->handleRequest($request);
        if(($contactForm->isSubmitted() && $contactForm->isValid())){

            $contactMessage = $contactForm->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contactMessage);
            $entityManager->flush();

            return $this->redirectToRoute('homePage', []);
        }

        return $this->render('pages/contact.html.twig', [
            'pageTitle' => 'Contact',
            'contactForm' => $contactForm->createView()
        ]);
    }

    /**
     * page de section
     * @Route("/section/{sectionName}", name="showBySection")
     */
    public function sectionPage($sectionName, ArticleRepository $articleRepository, Request $request,PaginatorInterface $paginator)
    {
        $donneesSection = $articleRepository->findBySectionPaginate($sectionName);
        if($donneesSection === null){
            throw new NotFoundHttpException('section does not exist');
        }

        $sectionPages = $paginator->paginate(
            $donneesSection,
            $request->query->getInt('page', 1),
            3
        );
        return $this->render('pages/section.html.twig',
            ['sectionPages' => $sectionPages]);

    }

    /**
     * page d'un article'
     * @Route("/article/{id<\d+>}", name="articlePage")
     */
    public function article($id, RomanRepository $romanRepo, ArticleRepository $articleRepo, Request $request, EntityManagerInterface $manager, CommentairesRepository $commentsRepo): Response
    {
        $article = $articleRepo->find($id);
        $roman = $romanRepo->findByArticleId($id);
        $allComments = $commentsRepo->findByArticleId($article);
        $comment = new Commentaires();
        $commentsForm = $this->createForm(CommentsType::class, $comment);
        $commentsForm->handleRequest($request);

        if($commentsForm->isSubmitted() && $commentsForm->isValid()){
            /** @var \App\Entity\User $user */
            $user = $this->getUser();
            $comment->setCreatedAt(new \DateTime())
                ->setIdArticle($article)
                ->setUserId($user);

            $manager->persist($comment);
            $manager->flush();
            return $this->redirectToRoute('articlePage',[
                'id'=> $article->getId()
            ]);
        }

        return $this->render('pages/article.html.twig', [
            'pageTitle' => 'titre d\'article',
            'article' => $article,
            'roman' => $roman,
            'comments' => $allComments,
            'commentsForm' => $commentsForm->createView()
        ]);
    }

    /**
     * @Route("/articleSearch", name="articleSearchPage")
     */
    public function recherche(Request $request, ArticleRepository $repo, PaginatorInterface $paginator)
    {
        $searchData = new Article();
        $searchForm = $this->createForm(SearchFormType::class, $searchData);
        $searchForm->handleRequest($request);

        $donnees = $repo->findAll();

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $donnees = $repo->searchDatas($searchData);

            if ($donnees == null) {
                $this->addFlash('erreur', 'Aucun article contenant ce mot clé dans le titre n\'a été trouvé, essayez en un autre.');

            }
        }

        $articlesSearch = $paginator->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            3
        );

        return $this->render('pages/resultatsRecherche.html.twig', [
            'pageTitle' => 'Résultats recherche',
            'articlesSearch' => $articlesSearch,
            'searchForm' => $searchForm->createView()
        ]);
    }




}

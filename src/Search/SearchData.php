<?php


namespace App\Search;


use Symfony\Component\Validator\Constraints as Assert;

class SearchData
{
    const SECTIONS = ['Critiques','Découvertes','Bizarreries'];

    /**
     * @var string
     */
    private $titre_article_data;

    /**
     * @var string
     * @Assert\Choice(choices=Article::SECTIONS, message="Le nom de section choisi est invalide")
     */
    private $section_data;

    public function getTitreArticleData(): ?string
    {
        return $this->titre_article_data;
    }

    public function setTitreArticleData(string $titre_article_data): self
    {
        $this->titre_article_data = $titre_article_data;

        return $this;
    }

    public function getSectionData(): ?string
    {
        return $this->section_data;
    }

    public function setSectionData(string $section_data): self
    {
        $this->section_data = $section_data;

        return $this;
    }
}
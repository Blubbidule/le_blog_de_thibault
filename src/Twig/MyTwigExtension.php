<?php


namespace App\Twig;


use App\Entity\Article;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MyTwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('getSections', [$this, 'getSections'])
        ];
    }

    public function getSections(){
        return Article::SECTIONS;
    }
}
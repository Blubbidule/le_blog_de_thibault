<?php

namespace App\DataFixtures;

use App\Entity\Roman;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Article;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i=1; $i<=12; $i++){
            $article = new Article();
            $article->setTitreArticle('Article sur le roman numéro '.$i)
                ->setTexteArticle('Contenu de l\'article n°'.$i)
                ->setImageArticle('webdev-5fd38e968ea4d.jpeg')
                ->setSection('Critiques')
                ->setCreatedAt(new \DateTime())
                ->setTextePresentation('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt ut. labore et dolore magna aliqua.
                                    Ut enim ad minim veniam')
                ->setSousTitreArticle('Sous-titre de l\'article n°'.$i);

            $roman = new Roman();
            $roman->setAuteur('Jean-yves')
                ->setEditionYear(2020)
                ->setGenres('Genre de type '.$i)
                ->setMaisonEdition('DuCarton')
                ->setTitreRoman('Titre du roman n°'.$i)
                ->setNote(rand(0,5))
                ->setIdArticle($article);

            $manager->persist($article);
            $manager->persist($roman);

        }
        for($i=13; $i<=19; $i++){
            $article = new Article();
            $article->setTitreArticle('Article sur le roman numéro '.$i)
                ->setTexteArticle('Contenu de l\'article n°'.$i)
                ->setImageArticle('1984.jpg')
                ->setSection('Découvertes')
                ->setCreatedAt(new \DateTime())
                ->setTextePresentation('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt ut. labore et dolore magna aliqua.
                                    Ut enim ad minim veniam')
                ->setSousTitreArticle('Sous-titre de l\'article n°'.$i);

            $roman = new Roman();
            $roman->setAuteur('Lance patates')
                ->setEditionYear(2020)
                ->setGenres('Genre de type '.$i)
                ->setMaisonEdition('Tartempion')
                ->setTitreRoman('Titre du roman n°'.$i)
                ->setNote(rand(0,5))
                ->setIdArticle($article);

            $manager->persist($article);
            $manager->persist($roman);

        }
        for($i=20; $i<=24; $i++){
            $article = new Article();
            $article->setTitreArticle('Article sur le roman numéro '.$i)
                ->setTexteArticle('Contenu de l\'article n°'.$i)
                ->setImageArticle('fahrenheit_451.jpg')
                ->setSection('Bizarreries')
                ->setCreatedAt(new \DateTime())
                ->setTextePresentation('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt ut. labore et dolore magna aliqua.
                                    Ut enim ad minim veniam')
                ->setSousTitreArticle('Sous-titre de l\'article n°'.$i);

            $roman = new Roman();
            $roman->setAuteur('Maboul Aroulé')
                ->setEditionYear(2020)
                ->setGenres('Genre de type '.$i)
                ->setMaisonEdition('LesFauchés')
                ->setTitreRoman('Titre du roman n°'.$i)
                ->setNote(rand(0,5))
                ->setIdArticle($article);

            $manager->persist($article);
            $manager->persist($roman);

        }

        $manager->flush();
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201207113915 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE roman ADD id_article_id INT NOT NULL');
        $this->addSql('ALTER TABLE roman ADD CONSTRAINT FK_B096ED2DD71E064B FOREIGN KEY (id_article_id) REFERENCES article (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B096ED2DD71E064B ON roman (id_article_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE roman DROP FOREIGN KEY FK_B096ED2DD71E064B');
        $this->addSql('DROP INDEX UNIQ_B096ED2DD71E064B ON roman');
        $this->addSql('ALTER TABLE roman DROP id_article_id');
    }
}

<?php


namespace App\Controller;

use App\Entity\Roman;
use App\Form\ArticleFormType;
use App\Form\RomanFormType;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Repository\RomanRepository;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class RomanController extends AbstractController
{

    /**
     * modification d'un roman dans db
     * @Route("/admin/article/roman/update/{id<\d+>}", name="updateRoman")
     */
    public function updateRoman(Roman $roman, Request $request, ArticleRepository $articleRepo): Response
    {

        if (!$roman){
            throw $this->createNotFoundException(
                'No book found '
            );
        }
        $article = $articleRepo->find($roman->getIdArticle());

        $romanForm = $this->createForm(RomanFormType::class, $roman);
        $romanForm->handleRequest($request);
        if($romanForm->isSubmitted() && $romanForm->isSubmitted()){
            $roman = $romanForm->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($roman);
            $entityManager->flush();

            return $this->redirectToRoute('articlePage', [
                'id' => $article->getId()
            ]);
        }

        return $this->render('pages/modificationRoman.html.twig', [
            'pageTitle' => 'Modification du roman',
            'romanForm' => $romanForm->createView()
        ]);

    }

    /**
     * suppression d'un roman dans db
     * @Route("/admin/article/roman/delete/{id<\d+>}", name="deleteRoman")
     */
    public function deleteRoman(int $id): Response
    {
        $romanRepo = $this->getDoctrine()->getRepository(Roman::class);
        $roman = $romanRepo->find($id);
        $entityManager = $this->getDoctrine()->getManager();

        if (!$roman){
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }

        $entityManager->remove($roman);
        $entityManager->flush();

        return $this->redirectToRoute('homePage',
            ['pageTitle' => 'Accueil']);

    }

}
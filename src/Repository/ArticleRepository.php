<?php

namespace App\Repository;

use App\Entity\Article;
use App\Search\SearchData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

        /**
     * @return \Doctrine\ORM\Query
     */
    public function searchDatas(Article $searchArticles) {
        $query = $this->createQueryBuilder('a');

        if(!empty($searchArticles->getTitreArticle())){
            $query = $query
                ->andWhere('a.titre_article LIKE :titre_article')
                ->setParameter('titre_article', '%'.$searchArticles->getTitreArticle().'%');
        }
        if(!empty($searchArticles->getSection())){
            $query = $query
                ->andWhere('a.section LIKE :section')
                ->setParameter('section', '%'.$searchArticles->getSection().'%');
        }

        return $query->getQuery()->execute();
    }


    /**
     * @param SearchData $searchArticles
     * @return \Doctrine\ORM\Query
     */
    public function search(SearchData $searchArticles) {
        $query = $this->createQueryBuilder('a');

        if(!empty($searchArticles->getTitreArticleData())){
            $query = $query
                ->andWhere('a.titre_article LIKE :titre_article_data')
                ->setParameter('titre_article', '%'.$searchArticles->getTitreArticleData().'%');
        }
        if(!empty($searchArticles->getSectionData())){
            $query = $query
                ->andWhere('a.section LIKE :section_data')
                ->setParameter('section', '%'.$searchArticles->getSectionData().'%');
        }

        return $query->getQuery()->execute();
    }


    public function findBySection($section){
        return $this->createQueryBuilder('s')
            ->andWhere('s.section = :val')
            ->setParameter('val', $section)
            ->orderBy('s.id', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function findBySectionPaginate($section){
        return $this->createQueryBuilder('s')
            ->andWhere('s.section = :val')
            ->setParameter('val', $section)
            ->orderBy('s.id', 'DESC')
            ->getQuery()
            ;
    }

    public function findBySectionLimited($section){
        return $this->createQueryBuilder('s')
            ->andWhere('s.section = :val')
            ->setParameter('val', $section)
            ->orderBy('s.id', 'DESC')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findMostRecent(){
        return $this->createQueryBuilder('s')
            ->orderBy('s.id', 'DESC')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function findAllByRecent(){
        return $this->createQueryBuilder('s')
            ->orderBy('s.id', 'DESC')
            ->getQuery()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

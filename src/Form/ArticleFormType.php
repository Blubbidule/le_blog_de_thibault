<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Roman;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ArticleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre_article')
            ->add('section', ChoiceType::class, [
                'choices' => [
                    'choix de la section :' => [
                        'Critiques' => 'Critiques',
                        'Découvertes' => 'Découvertes',
                        'Bizarreries' => 'Bizarreries'
                    ]
                ]
            ])
            ->add('textePresentation')
            ->add('sousTitreArticle')
            ->add('image_article', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/*'
                        ]
                    ])
                ]
            ])
            ->add('texte_article')
            ->add('romanId', RomanFormType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}

<?php


namespace App\Controller;

use App\Entity\Commentaires;
use App\Form\CommentsType;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Entity\User;
use App\Repository\CommentairesRepository;
use App\Repository\RomanRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use PhpParser\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class CommentairesController extends AbstractController
{

    /**
     * modification d'un commentaire dans db
     * @Route("/comment/update/{id<\d+>}", name="updateComment")
     */
    public function updateComment(int $id, ArticleRepository $articleRepo, Request $request, EntityManagerInterface $manager, CommentairesRepository $commentRepo): Response
    {
        $comment = $commentRepo->find($id);
        $article = $articleRepo->find($comment->getIdArticle());

        $commentsForm = $this->createForm(CommentsType::class, $comment);
        $commentsForm->handleRequest($request);

        if($commentsForm->isSubmitted() && $commentsForm->isValid()){
            $comment->setIdArticle($article)
                ->setUserId($comment->getUserId());

            $manager->persist($comment);
            $manager->flush();
            return $this->redirectToRoute('articlePage',[
                'id'=> $article->getId()
            ]);
        }

        return $this->render('pages/editComment.html.twig', [
            'pageTitle' => 'Modification de commentaire',
            'commentsForm' => $commentsForm->createView()
        ]);

    }

    /**
     * suppression d'un commentaire dans db
     * @Route("/comment/delete/{id<\d+>}", name="deleteComment")
     */
    public function deleteComment(int $id, ArticleRepository $articleRepo, CommentairesRepository $commentRepo): Response
    {
        $commentary = $commentRepo->find($id);
        $article = $articleRepo->find($commentary->getIdArticle());
        $entityManager = $this->getDoctrine()->getManager();

        if (!$commentary){
            throw $this->createNotFoundException(
                'No commentary found for id '.$id
            );
        }

        $entityManager->remove($commentary);
        $entityManager->flush();

        return $this->redirectToRoute('articlePage',[
            'id'=> $article->getId()
        ]);

    }

}
<?php


namespace App\Controller;

use App\Form\ArticleFormType;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Repository\CommentairesRepository;
use App\Repository\RomanRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends AbstractController
{
    /**
     * page de création d'article
     * @Route("/admin/article/new", name="articleCreationPage")
     * @Route("/admin/article/update/{id<\d+>}", name="articleUpdatePage")
     */
    public function articleForm(Article $article = null, Request $request): Response
    {

        if(!$article){
            $article = new Article();
        }

        $articleForm = $this->createForm(ArticleFormType::class, $article);

        $articleForm->handleRequest($request);
        if(($articleForm->isSubmitted() && $articleForm->isValid())){
            if(!$article->getId()){
                $article->setCreatedAt(new \DateTime());
            }

            $imageFile = $articleForm->get('image_article')->getData();
            if($imageFile){
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();
                try {
                    $imageFile->move(
                        $this->getParameter('images_directory'), $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                //$articleForm->getData()->setImageArticle($newFilename);
                $article->setImageArticle($newFilename);

            }

            $article = $articleForm->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('articlePage', [
                'id' => $article->getId()
            ]);
        }


        return $this->render('pages/creationArticle.html.twig', [
            'pageTitle' => 'Création d\'article',
            'articleForm' => $articleForm->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }

    /**
     * suppression d'article dans db
     * @Route("/admin/article/delete/{id<\d+>}", name="deleteArticle")
     */
    public function deleteArticle(int $id, ArticleRepository $articleRepo, RomanRepository $romanRepo, CommentairesRepository $commentRepo): Response
    {
        $article = $articleRepo->find($id);
        $roman = $romanRepo->findByArticleId($article);
        $comments = $commentRepo->findByArticleId($article);
        $entityManager = $this->getDoctrine()->getManager();

        if (!$article){
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }

        $entityManager->remove($article);
        $entityManager->remove($roman);
        foreach ($comments as $comment){
            $entityManager->remove($comment);
        }

        $entityManager->flush();

        return $this->redirectToRoute('homePage',
            ['pageTitle' => 'Accueil']);

    }

}
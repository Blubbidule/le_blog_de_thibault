<?php


namespace App\Controller;


use App\Entity\ContactMessage;
use App\Form\ContactType;
use App\Repository\ContactMessageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * page de contact
     * @Route("/admin/contactMessage/delete/{id<\d+>}", name="contactMessageDeletePage")
     */
    public function contact(int $id, ContactMessageRepository $contactRepo): Response
    {
        $contactMessage = $contactRepo->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($contactMessage);
        $entityManager->flush();

        return $this->redirectToRoute('adminGestionContactPage', []);
    }
}
<?php


namespace App\Controller;

use App\Entity\User;
use App\Form\AdminUserType;
use App\Form\EditProfileFormType;
use App\Form\RegistrationType;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Repository\CommentairesRepository;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use App\Service\ImageUploader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class UserController extends AbstractController
{
    /**
     * ajout d'utilisateur dans db
     * @Route("/registration", name="registrationPage")
     */
    public function newUser(Request $request, ImageUploader $imageUploader, UserPasswordEncoderInterface $userEncoder): Response
    {

        $newUser = new User();
        $newUser->setRoles(['ROLE_USER'])
            ->setActive(false)
            ->setLocked(false)
            ->setProfileImage('user_icon.png');
        $userForm = $this->createForm(RegistrationType::class, $newUser);
        $userForm->handleRequest($request);
        if($userForm->isSubmitted() && $userForm->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $hash = $userEncoder->encodePassword($newUser, $newUser->getPassword());
            $newUser->setPassword($hash);

            $entityManager->persist($newUser);
            $entityManager->flush();
            return $this->redirectToRoute('connectionPage', []);
        }

        return $this->render('pages/inscription.html.twig', [
            'pageTitle' => 'Inscription',
            'userForm' => $userForm->createView()
        ]);


    }

    /**
     * page de modification de profil
     * @Route("/profile/update/{id<\d+>}", name="updateUser")
     */
    public function updateUser(User $user, Request $request, ImageUploader $imageUploader, UserPasswordEncoderInterface $userEncoder): Response
    {
        $userForm = $this->createForm(EditProfileFormType::class, $user);
        $userForm->handleRequest($request);
        $user->setActive($user->getActive())
            ->setLocked($user->getLocked())
            ->setPassword($user->getPassword());


        if($userForm->isSubmitted() && $userForm->isValid()){
            /** @var UploadedFile $avatarFile */
            $user = $userForm->getData();
            $avatarFile = $userForm->get('profileImage')->getData();
            if($avatarFile){
                $avatarFileName = $imageUploader->uploadImageFromForm($avatarFile);
                $user->setProfileImage($avatarFileName);
            }
            $hash = $userEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('profilePage', []);
        }

        return $this->render('pages/changerProfil.html.twig', [
            'pageTitle' => 'Modification profil',
            'userForm' => $userForm->createView()
        ]);

    }

    /**
     * page de modification d'utilisateur (par admin)
     * @Route("/admin/user/update/{id<\d+>}", name="updateUserAdmin")
     */
    public function updateUserAdmin(User $user, Request $request, ImageUploader $imageUploader, UserPasswordEncoderInterface $userEncoder): Response
    {
        $userForm = $this->createForm(AdminUserType::class, $user);
        $userForm->handleRequest($request);


        if($userForm->isSubmitted() && $userForm->isValid()){
            /** @var UploadedFile $avatarFile */
            $user = $userForm->getData();
            $avatarFile = $userForm->get('profileImage')->getData();
            if($avatarFile){
                $avatarFileName = $imageUploader->uploadImageFromForm($avatarFile);
                $user->setProfileImage($avatarFileName);
            }
            $hash = $userEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('adminGestionUserPage', []);
        }

        return $this->render('pages/changerProfil.html.twig', [
            'pageTitle' => 'Modification profil',
            'userForm' => $userForm->createView()
        ]);

    }

    /**
     * suppression d'utilisateur dans db
     * @Route("/admin/user/delete/{id<\d+>}", name="deleteUser")
     */
    public function deleteUser(int $id, CommentairesRepository $commentRepo): Response
    {
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepo->find($id);
        $comments = $commentRepo->findByUserId($user);
        $users = $userRepo->findAll();
        $entityManager = $this->getDoctrine()->getManager();

        if (!$user){
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }

        $entityManager->remove($user);
        foreach ($comments as $comment){
            $entityManager->remove($comment);
        }
        $entityManager->flush();

        return $this->redirectToRoute('adminGestionUserPage', [
            'pageTitle' => 'Gestion Admin',
            'users' => $users

        ]);

    }

    /**
     * page de connexion
     * @Route("/connection", name="connectionPage")
     */
    public function connection(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('pages/connection.html.twig', [
            'pageTitle' => 'Connexion',
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {

    }

    /**
     * Création d'utilisateur par admin
     * @Route("/admin/userCreation", name="userCreationPage")
     */
    public function newUserByAdmin(Request $request, ImageUploader $imageUploader, UserPasswordEncoderInterface $userEncoder): Response
    {

        $newUser = new User();
        $userForm = $this->createForm(AdminUserType::class, $newUser);
        $userForm->handleRequest($request);
        if($userForm->isSubmitted() && $userForm->isValid()){
            /** @var UploadedFile $avatarFile */
            $newUser = $userForm->getData();
            $avatarFile = $userForm->get('profileImage')->getData();
            if($avatarFile){
                $avatarFileName = $imageUploader->uploadImageFromForm($avatarFile);
                $newUser->setProfileImage($avatarFileName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $hash = $userEncoder->encodePassword($newUser, $newUser->getPassword());
            $newUser->setPassword($hash);

            $entityManager->persist($newUser);
            $entityManager->flush();
            return $this->redirectToRoute('adminGestionUserPage', []);
        }

        return $this->render('pages/adminModifUser.html.twig', [
            'pageTitle' => 'Création user',
            'userForm' => $userForm->createView()
        ]);


    }

    /**
     * page de profil
     * @Route("/profile", name="profilePage")
     */
    public function profile(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        return $this->render('pages/profil.html.twig', [
            'pageTitle' => 'Profil',
            'userData' => $user
        ]);
    }

    /**
     * page de lien vers attente activation profil
     * @Route("/waitingActivateProfile", name="waitingProfileActivationPage")
     */
    public function waitingProfileActivation(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        return $this->render('pages/waitingRegistration.html.twig', [
            'pageTitle' => 'Attente activation compte',
            'user' => $user
        ]);
    }

    /**
     * page de lien vers activation profil
     * @Route("/activateProfile", name="profileActLinkPage")
     */
    public function profileActLink(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        return $this->render('pages/activateAccount.html.twig', [
            'pageTitle' => 'Lien vers activation compte',
            'user' => $user
        ]);
    }

    /**
     * @param User $user
     * @Route("/activate/mail", name="mailSend")
     */
    public function activateAccountByMail(string $id, UserRepository $repository, EntityManagerInterface $entityManager, GuardAuthenticatorHandler $guardAuthenticatorHandler, Request $request, LoginFormAuthenticator $authenticator) {
        /**
         * @var User $user
         */
        $user = $repository->findOneById($id);

        if ($user !== null) {
            $user->setActive(true);
            $entityManager->flush();

        }

        return $guardAuthenticatorHandler->authenticateUserAndHandleSuccess(
            $user,
            $request,
            $authenticator,
            'main' // firewall name in security.yaml
        );

    }

    /**
     * @param User $user
     * @Route("/activate/{id<\d+>}", name="activateAccount")
     */
    public function activateAccount(string $id, UserRepository $repository, EntityManagerInterface $entityManager, GuardAuthenticatorHandler $guardAuthenticatorHandler, Request $request, LoginFormAuthenticator $authenticator) {
        /**
         * @var User $user
         */
        $user = $repository->findOneById($id);

        if ($user !== null) {
            $user->setActive(true);
            $entityManager->flush();

        }

        return $guardAuthenticatorHandler->authenticateUserAndHandleSuccess(
            $user,
            $request,
            $authenticator,
            'main' // firewall name in security.yaml
        );

    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', ['error' => $error]);
    }

}
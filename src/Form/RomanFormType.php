<?php

namespace App\Form;

use App\Entity\Roman;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RomanFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titreRoman')
            ->add('auteur')
            ->add('genres')
            ->add('maisonEdition')
            ->add('editionYear')
            ->add('note', ChoiceType::class, [
                'choices' => [
                    'Note du roman :' => [
                        'Très mauvais (0 sur 5)' => 0,
                        'Mauvais (1 sur 5)' => 1,
                        'Passable (2 sur 5)' => 2,
                        'Bon (3 sur 5)' => 3,
                        'Très Bon (4 sur 5)' => 4,
                        'Excellent (5 sur 5)' => 5
                    ]
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Roman::class,
        ]);
    }
}

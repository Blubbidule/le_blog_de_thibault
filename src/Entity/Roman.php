<?php

namespace App\Entity;

use App\Repository\RomanRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RomanRepository::class)
 */
class Roman
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titreRoman;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $auteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $genres;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $maisonEdition;


    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *     min=0,
     *     max=5,
     *     notInRangeMessage="La note doit être entre 0 et 5",
     * )
     */
    private $note;

    /**
     * @ORM\OneToOne(targetEntity=Article::class, inversedBy="romanId", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $idArticle;

    /**
     * @ORM\Column(type="integer")
     */
    private $editionYear;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreRoman(): ?string
    {
        return $this->titreRoman;
    }

    public function setTitreRoman(string $titreRoman): self
    {
        $this->titreRoman = $titreRoman;

        return $this;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getGenres(): ?string
    {
        return $this->genres;
    }

    public function setGenres(string $genres): self
    {
        $this->genres = $genres;

        return $this;
    }

    public function getMaisonEdition(): ?string
    {
        return $this->maisonEdition;
    }

    public function setMaisonEdition(string $maisonEdition): self
    {
        $this->maisonEdition = $maisonEdition;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getIdArticle(): ?Article
    {
        return $this->idArticle;
    }

    public function setIdArticle(Article $idArticle): self
    {
        $this->idArticle = $idArticle;

        return $this;
    }

    public function getEditionYear(): ?int
    {
        return $this->editionYear;
    }

    public function setEditionYear(int $editionYear): self
    {
        $this->editionYear = $editionYear;

        return $this;
    }
}

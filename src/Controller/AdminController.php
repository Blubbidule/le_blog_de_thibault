<?php


namespace App\Controller;


use App\Entity\Roman;
use App\Form\RomanFormType;
use App\Repository\ContactMessageRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ContactMessage;
use App\Form\ArticleFormType;
use App\Form\CommentsType;
use App\Form\ContactType;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Repository\CommentairesRepository;
use App\Repository\RomanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Commentaires;

class AdminController extends AbstractController
{
    /**
     * page de gestion admin (Articles)
     * @Route("/admin/gestion/articles", name="adminGestionArticlesPage")
     */
    public function adminActions(ArticleRepository $artRepo): Response
    {
        $articles = $artRepo->findAll();

        return $this->render('admin/tableGestionAdmin.html.twig', [
            'pageTitle' => 'Gestion Admin',
            'Articles' => $articles
        ]);
    }

    /**
     * page de gestion admin (commentaires)
     * @Route("/admin/gestion/commentaires", name="adminGestionCommPage")
     */
    public function adminActionsComments(CommentairesRepository $commentRepo): Response
    {
        $comments = $commentRepo->findAll();

        return $this->render('admin/tableGestionAdmin.html.twig', [
            'pageTitle' => 'Gestion Admin',
            'comments' => $comments
        ]);
    }

    /**
     * page de gestion admin (message contact)
     * @Route("/admin/gestion/contact", name="adminGestionContactPage")
     */
    public function adminActionsContact(ContactMessageRepository $contactRepo): Response
    {
        $contactMessages = $contactRepo->findAll();

        return $this->render('admin/tableGestionAdmin.html.twig', [
            'pageTitle' => 'Gestion Admin',
            'contacts' => $contactMessages
        ]);
    }

    /**
     * page de gestion admin (romans)
     * @Route("/admin/gestion/romans", name="adminGestionRomanPage")
     */
    public function adminActionsRoman(RomanRepository $romanRepo): Response
    {
        $romans = $romanRepo->findAll();

        return $this->render('admin/tableGestionAdmin.html.twig', [
            'pageTitle' => 'Gestion Admin',
            'romans' => $romans
        ]);
    }

    /**
     * page de gestion admin (user)
     * @Route("/admin/gestion/user", name="adminGestionUserPage")
     */
    public function adminActionsUser(UserRepository $userRepo): Response
    {
        $users = $userRepo->findAll();

        return $this->render('admin/tableGestionAdmin.html.twig', [
            'pageTitle' => 'Gestion Admin',
            'users' => $users
        ]);
    }

    /**
     * page de création d'article
     * @Route("/admin/adminArticle/new", name="articleAdminCreationPage")
     * @Route("/admin/adminArticle/update/{id<\d+>}", name="articleAdminUpdatePage")
     */
    public function adminArticleForm(Article $article = null, Request $request, ArticleRepository $artRepo): Response
    {
        $articles = $artRepo->findAll();
        if(!$article){
            $article = new Article();
        }

        $articleForm = $this->createForm(ArticleFormType::class, $article);

        $articleForm->handleRequest($request);
        if(($articleForm->isSubmitted() && $articleForm->isValid())){
            if(!$article->getId()){
                $article->setCreatedAt(new \DateTime());
            }

            $imageFile = $articleForm->get('image_article')->getData();
            if($imageFile){
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();
                try {
                    $imageFile->move(
                        $this->getParameter('images_directory'), $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                //$articleForm->getData()->setImageArticle($newFilename);
                $article->setImageArticle($newFilename);

            }

            $article = $articleForm->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('adminGestionArticlesPage', [
                'Articles' => $articles
            ]);
        }


        return $this->render('pages/creationArticle.html.twig', [
            'pageTitle' => 'Création d\'article',
            'articleForm' => $articleForm->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }

    /**
     * suppression d'article dans db
     * @Route("/admin/adminArticle/delete/{id<\d+>}", name="adminDeleteArticle")
     */
    public function adminDeleteArticle(int $id, ArticleRepository $articleRepo, RomanRepository $romanRepo, CommentairesRepository $commentRepo): Response
    {
        $articles = $articleRepo->findAll();
        $article = $articleRepo->find($id);
        $roman = $romanRepo->findByArticleId($article);
        $comments = $commentRepo->findByArticleId($article);
        $entityManager = $this->getDoctrine()->getManager();

        if (!$article){
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }

        $entityManager->remove($article);
        $entityManager->remove($roman);
        foreach ($comments as $comment){
            $entityManager->remove($comment);
        }

        $entityManager->flush();

        return $this->redirectToRoute('adminGestionArticlesPage', [
            'Articles' => $articles
        ]);

    }

    /**
     * @Route("/admin/contactMessage/create", name="adminContactPage")
     * @Route("/admin/contactMessage/update/{id<\d+>}", name="contactMessageUpdatePage")
     */
    public function contact(ContactMessage $contactMessage = null, Request $request, ContactMessageRepository $contactRepo): Response
    {
        if(!$contactMessage){
            $contactMessage = new ContactMessage();
        }

        $contactForm = $this->createForm(ContactType::class, $contactMessage);
        $contactMessages = $contactRepo->findAll();
        $contactForm->handleRequest($request);
        if(($contactForm->isSubmitted() && $contactForm->isValid())){

            $contactMessage = $contactForm->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contactMessage);
            $entityManager->flush();

            return $this->redirectToRoute('adminGestionContactPage', [
                'contacts' => $contactMessages
            ]);
        }

        return $this->render('pages/contact.html.twig', [
            'pageTitle' => 'Contact',
            'contactForm' => $contactForm->createView()
        ]);
    }

    /**
     * modification d'un commentaire dans db
     * @Route("/admin/comment/update/{id<\d+>}", name="adminUpdateComment")
     */
    public function adminUpdateComment(int $id, ArticleRepository $articleRepo, Request $request, EntityManagerInterface $manager, CommentairesRepository $commentRepo): Response
    {
        $comment = $commentRepo->find($id);
        $article = $articleRepo->find($comment->getIdArticle());
        $comments = $commentRepo->findAll();

        $commentsForm = $this->createForm(CommentsType::class, $comment);
        $commentsForm->handleRequest($request);

        if($commentsForm->isSubmitted() && $commentsForm->isValid()){
            $comment->setIdArticle($article)
                ->setUserId($comment->getUserId());

            $manager->persist($comment);
            $manager->flush();
            return $this->redirectToRoute('adminGestionCommPage',[
                'comments' => $comments
            ]);
        }

        return $this->render('pages/editComment.html.twig', [
            'pageTitle' => 'Modification de commentaire',
            'commentsForm' => $commentsForm->createView()
        ]);

    }

    /**
     * suppression d'un commentaire dans db
     * @Route("/admin/comment/delete/{id<\d+>}", name="adminDeleteComment")
     */
    public function adminDeleteComment(int $id, CommentairesRepository $commentRepo): Response
    {
        $commentary = $commentRepo->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $comments = $commentRepo->findAll();

        if (!$commentary){
            throw $this->createNotFoundException(
                'No commentary found for id '.$id
            );
        }

        $entityManager->remove($commentary);
        $entityManager->flush();

        return $this->redirectToRoute('adminGestionCommPage',[
            'comments' => $comments
        ]);

    }

    /**
     * modification d'un roman dans db
     * @Route("/admin/roman/update/{id<\d+>}", name="adminUpdateRoman")
     */
    public function updateRoman(Roman $roman, Request $request, RomanRepository $romanRepo): Response
    {
        $romanForm = $this->createForm(RomanFormType::class, $roman);
        $romanForm->handleRequest($request);
        $romans = $romanRepo->findAll();

        if($romanForm->isSubmitted() && $romanForm->isSubmitted()){
            $roman = $romanForm->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($roman);
            $entityManager->flush();


            return $this->redirectToRoute('adminGestionRomanPage',[
                'romans' => $romans
            ]);
        }

        return $this->render('pages/modificationRoman.html.twig', [
            'pageTitle' => 'Modification du roman',
            'romanForm' => $romanForm->createView()
        ]);

    }

}